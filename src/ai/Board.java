package ai;

import game.components.Rect;
import game.data.Move;
import game.data.PoleDirection;
import main.genome.GenomePropagator;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static ai.Utils.*;

public class Board {
    private final int width;
    private final int height;

    private Rect nextFood;
    private LinkedList<Rect> snake;

    private Move currentMove;
    private PoleDirection poleDirection;
    private boolean removeTailNextTime = false;
    private boolean gameOver = false;


    public Board(int width, int height) {
        this.width = width;
        this.height = height;
        this.reset();
    }

    public int snakeSize() {
        return snake.size();
    }

    public Rect traverse(Rect root) {
        switch (this.poleDirection) {
            case NORTH:
                return new Rect(root.x, root.y - 1, 1, 1);
            case WEST:
                return new Rect(root.x + 1, root.y, 1, 1);
            case SOUTH:
                return new Rect(root.x, root.y + 1, 1, 1);
            case EAST:
                return new Rect(root.x - 1, root.y, 1, 1);
        }
        return null;
    }

    public Rect traverse(Rect root, Move control) {
        switch (transformDirection(this.poleDirection, control)) {
            case NORTH:
                return new Rect(root.x, root.y - 1, 1, 1);
            case WEST:
                return new Rect(root.x + 1, root.y, 1, 1);
            case SOUTH:
                return new Rect(root.x, root.y + 1, 1, 1);
            case EAST:
                return new Rect(root.x - 1, root.y, 1, 1);
        }
        return null;
    }

    public PoleDirection transformDirection(PoleDirection direction, Move control) {
        return getDirectionFromMove(direction, control);
    }

    public boolean isObstacle(Rect field) {
        boolean isObstacle = false;
        for(Rect partBody : snake) {
            if(snake.getFirst() == partBody) { //if this is the head skip
                continue;
            }
            if(partBody.x == field.x && partBody.y == field.y) {
                isObstacle = true;
            }
        }
        return snake.lastIndexOf(field) > 0 || field.x < 0 || field.x >= width || field.y < 0 || field.y >= height || isObstacle;
    }

    /**
     * index       |       meaning
     * ---------------------------
     * 0       |       obstacle to the left
     * 1       |       obstacle in front
     * 2       |       obstacle to the right
     * 3       |       angle to the food
     *
     * @return
     */
    public List<Float> extractNetworkInput() {

        List<Float> out = new ArrayList<>();

        out.add((float) (isObstacle(traverse(snake.getFirst(), Move.LEFT)) ? 1 : 0));
        out.add((float) (isObstacle(traverse(snake.getFirst(), Move.STRAIGHT)) ? 1 : 0));
        out.add((float) (isObstacle(traverse(snake.getFirst(), Move.RIGHT)) ? 1 : 0));
        out.add(distanceSnakeAppleLeft(snake.getFirst(), nextFood, this.poleDirection));
        out.add(distanceSnakeAppleStraight(snake.getFirst(), nextFood, this.poleDirection));
        out.add(distanceSnakeAppleRight(snake.getFirst(), nextFood, this.poleDirection));
        out.add(distanceSnakeAppleTopLeft(snake.getFirst(), nextFood, this.poleDirection));
        out.add(distanceSnakeAppleTopRight(snake.getFirst(), nextFood, this.poleDirection));
        out.add(distanceSnakeAppleBottomLeft(snake.getFirst(), nextFood, this.poleDirection));
        out.add(distanceSnakeAppleBottomRight(snake.getFirst(), nextFood, this.poleDirection));
        out.add(changeMoveToInt(currentMove));
        return out;
    }

    public void reset() {
        this.snake = new LinkedList<>();
        this.snake.add(new Rect(width / 2, height / 2, 1, 1));
        this.currentMove = null;
        this.poleDirection =  PoleDirection.NORTH;

        this.removeTailNextTime = false;

        this.gameOver = false;

        newFoodTarget();
    }

    public void newFoodTarget() {
        Rect next = new Rect((int) (this.width * Math.random()), (int) (this.width * Math.random()), 1, 1);
        while (snake.contains(next)) {
            next = new Rect((int) (this.width * Math.random()), (int) (this.width * Math.random()), 1, 1);
        }
        nextFood = next;
    }

    public Move directionFromOutput(List<Float> out) {
        int index = 0;
        for (int i = 1; i < out.size(); i++) {
            if (out.get(i) > out.get(index)) {
                index = i;
            }
        }
        return changeIntToMove(index - 1);
    }

    public void move(GenomePropagator client) {
        List<Float> out = client.propagate(this.extractNetworkInput());
        Move moveChoosen = directionFromOutput(out);
        this.move(moveChoosen);
    }

    /**
     * 1 to move left
     * 0 to move straight
     * -1 to move right
     *
     * @param direction
     */
    public void move(Move direction) {
        if (gameOver) return;
        synchronized (snake) {
            this.poleDirection = getDirectionFromMove(this.poleDirection, direction);
            Rect newHead = traverse(snake.getFirst());
            snake.addFirst(newHead);
            if (removeTailNextTime) {
                snake.removeLast();
            } else {
                removeTailNextTime = true;
            }

            if (newHead.equals(nextFood)) {
                newFoodTarget();
                removeTailNextTime = false;
            }

            if (isObstacle(newHead)) {
                gameOver = true;
            }
        }
    }

    // GETTERS & SETTERS
    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public Rect getNextFood() {
        return nextFood;
    }

    public LinkedList<Rect> getSnake() {
        return snake;
    }

    public boolean isGameOver() {
        return gameOver;
    }
}
