package ai;

import main.Neat;
import main.evaluator.SimpleEvaluator;
import main.generation.Generation;
import main.generation.GenerationFactory;
import main.genome.*;

public class Training {
    private Board board;
    private final static int MAX_GENERATION = 300;

    private final GenomeEvaluator evaluator = new SimpleEvaluator() {
        @Override
        public float evaluate(Genome genome) {
            return rateClient(board, genome);
        }
    };

    public static int rateClient(Board board, Genome client) {
        board.reset();
        GenomePropagator propagator = new GenomePropagator(client);

        int iteration = 0;
        int iterationsWithoutFood = 0;
        int food = 0;
        while (iteration < 1000 && iterationsWithoutFood < 20 && !board.isGameOver()) {
            iteration++;
            board.move(propagator);
            if (board.snakeSize() != food) {
                iterationsWithoutFood = 0;
                food = board.snakeSize();
            } else {
                iterationsWithoutFood++;
            }
        }
        return (board.snakeSize());

    }

    public GenomePropagator start() {

        board = new Board(10, 10);

        Neat neat = Neat.create(evaluator, 11,3);
        neat.getSpeciesFactory().setDistanceThreshold(3);
        neat.getGenerationFactory().setPopulationSize(300);

        // Config mutator
        GenomeMutator mutator = neat.getMutator();
        mutator.setNodeMutationProbability(0.1f);
        mutator.setConnectionMutationProbability(0.3f);
        mutator.setWeightMutationProbability(0.4f);

        GenomeFactory factory = neat.getGenomeFactory();
        factory.setReexpressProbability(0.2f);


        // Config comparator
        GenomeComparator comparator = neat.getGenomeComparator();
        comparator.setDisjointFactor(0.1f);
        comparator.setExcessFactor(0.1f);
        comparator.setWeightDiffFactor(0.06f);

        Generation generation = null;

        for (int i = 0; i < MAX_GENERATION; i++) {
            System.out.println("#################### " + i + " ######################");
            generation = neat.evolve();
            System.out.println("Average score : " + generation.getTotalFitness());
            System.out.println("Best score : " + generation.getBestGenome().getFitness());
        }

        Genome bestGenome = generation.getBestGenome();

        return new GenomePropagator(bestGenome);
    }
}
