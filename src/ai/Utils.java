package ai;

import game.components.Rect;
import game.data.Move;
import game.data.PoleDirection;

public class Utils {
    public static PoleDirection changeIntToDirection(int direction) {
        switch (direction) {
            case 0:
                return PoleDirection.NORTH;
            case 1:
                return PoleDirection.WEST;
            case 2:
                return PoleDirection.SOUTH;
            case 3:
                return PoleDirection.EAST;
        }
        return null;
    }

    public static int changeDirectionToInt(PoleDirection direction) {
        switch (direction) {
            case NORTH:
                return 0;
            case WEST:
                return 1;
            case SOUTH:
                return 2;
            case EAST:
                return 3;
        }
        return 0;
    }

    public static Move changeIntToMove(int move) {
        switch (move) {
            case -1:
                return Move.RIGHT;
            case 0:
                return Move.STRAIGHT;
            case 1:
                return Move.LEFT;
        }
        return null;
    }

    public static float changeMoveToInt(Move move) {
        if (move != null) {
            switch (move) {
                case RIGHT:
                    return -1;
                case STRAIGHT:
                    return 0;
                case LEFT:
                    return 1;
            }
        }
        return 0;
    }

    public static PoleDirection getDirectionFromMove(PoleDirection currentDirection, Move move) {
        switch (currentDirection) {
            case NORTH:
                switch (move) {
                    case RIGHT:
                        return PoleDirection.WEST;
                    case STRAIGHT:
                        return PoleDirection.NORTH;
                    case LEFT:
                        return PoleDirection.EAST;
                }
            case WEST:
                switch (move) {
                    case RIGHT:
                        return PoleDirection.SOUTH;
                    case STRAIGHT:
                        return PoleDirection.WEST;
                    case LEFT:
                        return PoleDirection.NORTH;
                }
            case SOUTH:
                switch (move) {
                    case RIGHT:
                        return PoleDirection.EAST;
                    case STRAIGHT:
                        return PoleDirection.SOUTH;
                    case LEFT:
                        return PoleDirection.WEST;
                }
            case EAST:
                switch (move) {
                    case RIGHT:
                        return PoleDirection.NORTH;
                    case STRAIGHT:
                        return PoleDirection.EAST;
                    case LEFT:
                        return PoleDirection.SOUTH;
                }
        }
        return null;
    }

    public static float distanceSnakeAppleStraight(Rect head, Rect food, PoleDirection direction) {
        switch (direction) {
            case NORTH:
                return head.x == food.x && head.y >= food.y ? (int) (head.y - food.y) : -1;
            case WEST:
                return head.y == food.y && head.x <= food.x ? (int) (food.x - head.x) : -1;
            case SOUTH:
                return head.x == food.x && head.y <= food.y ? (int) (food.y - head.y) : -1;
            case EAST:
                return head.y == food.y && head.x >= food.x ? (int) (head.x - food.x) : -1;
        }
        return 0;
    }

    public static float distanceSnakeAppleLeft(Rect head, Rect food, PoleDirection direction) {
        switch (direction) {
            case NORTH:
                return head.x >= food.x && head.y == food.y ? (int) (head.x - food.x) : -1;
            case WEST:
                return head.y >= food.y && head.x == food.x ? (int) (head.y - food.y) : -1;
            case SOUTH:
                return head.x <= food.x && head.y == food.y ? (int) (food.x - head.x) : -1;
            case EAST:
                return head.y <= food.y && head.x == food.x ? (int) (food.y - head.y) : -1;
        }
        return 0;
    }

    public static float distanceSnakeAppleRight(Rect head, Rect food, PoleDirection direction) {
        switch (direction) {
            case NORTH:
                return head.x <= food.y && head.y == food.y ? (int) (food.x - head.x) : -1;
            case WEST:
                return head.y <= food.y && head.x == food.x ? (int) (food.y - head.y) : -1;
            case SOUTH:
                return head.x >= food.x && head.y == food.y ? (int) (head.y + food.y) : -1;
            case EAST:
                return head.y >= food.y && head.x == food.x ? (int) (head.y - food.y) : -1;
        }
        return 0;
    }

    public static float distanceSnakeAppleTopLeft(Rect head, Rect food, PoleDirection direction) {
        switch (direction) {
            case NORTH:
                if (head.x - food.x == head.y - food.y && head.y > food.y && head.x > food.x) {
                    return (int) (head.x - food.x);
                }
                break;
            case WEST:
                if (food.x - head.x == head.y - food.y && head.y > food.y && head.x < food.x) {
                    return (int) (food.x - head.x);
                }
                break;
            case SOUTH:
                if (food.x - head.x == food.y - head.y && head.y < food.y && head.x < food.x) {
                    return (int) (food.x - head.x);
                }
                break;
            case EAST:
                if (food.y - head.y == head.x - food.x && head.y < food.y && head.x > food.x) {
                    return (int) (head.x - food.x);
                }
                break;
        }
        return 0;
    }

    public static float distanceSnakeAppleTopRight(Rect head, Rect food, PoleDirection direction) {
        switch (direction) {
            case NORTH:
                if (food.x - head.x == head.y - food.y && head.y > food.y && head.x < food.x) {
                    return (int) (head.y - food.y);
                }
                break;
            case WEST:
                if (food.x - head.x == food.y - head.y && head.y < food.y && head.x < food.x) {
                    return (int) (food.y - head.y);
                }
                break;
            case SOUTH:
                if (head.x - food.x == food.y - head.y && head.y < food.y && head.x > food.x) {
                    return (int) (food.y - head.y);
                }
                break;
            case EAST:
                if (head.y - food.y == head.x - food.x && head.y > food.y && head.x > food.x) {
                    return (int) (head.y - food.y);
                }
                break;
        }
        return 0;
    }

    public static float distanceSnakeAppleBottomLeft(Rect head, Rect food, PoleDirection direction) {
        switch (direction) {
            case NORTH, WEST:
                if (head.x - food.x == head.y - food.y && head.y > food.y && head.x > food.x) {
                    return (int) (head.x - food.x);
                }
                break;
            case SOUTH:
                if (food.x - head.x == head.y - food.y && head.y > food.y && head.x < food.x) {
                    return (int) (food.x - head.x);
                }
                break;
            case EAST:
                if (food.x - head.x == food.y - head.y && head.y < food.y && head.x < food.x) {
                    return (int) (food.x - head.x);
                }
                break;
        }
        return 0;
    }

    public static float distanceSnakeAppleBottomRight(Rect head, Rect food, PoleDirection direction) {
        switch (direction) {
            case NORTH:
                if (food.x - head.x == food.y - head.y && head.y < food.y && head.x < food.x) {
                    return (int) (food.y - head.y);
                }
                break;
            case WEST:
                if (head.x - food.x == food.y - head.y && head.y < food.y && head.x > food.x) {
                    return (int) (food.y - head.y);
                }
                break;
            case SOUTH:
                if (head.x - food.x == head.y - food.y && head.y > food.y && head.x > food.x) {
                    return (int) (head.y - food.y);
                }
                break;
            case EAST:
                if (food.x - head.x == head.y - food.y && head.y > food.y && head.x < food.x) {
                    return (int) (head.y - food.y);
                }
                break;
        }
        return 0;
    }
}
