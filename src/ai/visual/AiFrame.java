package ai.visual;

import ai.Board;
import ai.Training;
import game.components.Rect;
import game.data.Constants;
import game.scene.Scene;
import main.genome.GenomePropagator;
import java.awt.*;
import java.awt.geom.Rectangle2D;

public class AiFrame extends Scene {

    private GenomePropagator client;
    private Rect background, foreground;
    private Board board;
    public double ogWaitBetweenUpdates = Constants.OG_BETWEEN_TIME;
    public double waitTimeLeft = ogWaitBetweenUpdates;

    public AiFrame(Board board) {
        Training training = new Training();
        client = training.start();
        background = new Rect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
        foreground = new Rect(24, 24, Constants.TILE_WIDTH * 25, Constants.TILE_WIDTH * 25);
        this.board = board;
        board.reset();
    }

    @Override
    public void update(double dt) {
        if (waitTimeLeft > 0) {
            waitTimeLeft -= dt;
            return;
        }
        waitTimeLeft = ogWaitBetweenUpdates;
        if(!board.isGameOver()) {
            board.move(client);
        }
    }

    @Override
    public void draw(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(Color.black);
        g2d.fill(new Rectangle2D.Double(background.x, background.y, background.width, background.height));
        g2d.setColor(Color.white);
        g2d.fill(new Rectangle2D.Double(foreground.x, foreground.y, foreground.width, foreground.height));

        //render snake
        for(Rect f: board.getSnake()){
            renderField(Color.darkGray, f, g2d);
        }
        renderField(Color.gray, board.getSnake().getFirst(), g2d);

        //render food
        renderField(Color.red, board.getNextFood(), g2d);
    }

    private void renderField(Color c, Rect f, Graphics2D graphics){
        int x = (int) (24 + (f.x * 24));
        int y = (int) (24 + (f.y * 24));
        int width = 24;
        int height = 24;

        graphics.setColor(c);
        graphics.fillRect(x, y,width, height);
    }
}
