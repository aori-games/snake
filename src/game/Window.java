package game;

import ai.Board;
import ai.visual.AiFrame;
import game.listener.KL;
import game.listener.ML;
import game.scene.*;
import game.data.Constants;
import neat.Client;

import javax.swing.JFrame;
import java.awt.*;
import java.time.Duration;
import java.time.Instant;

public class Window extends JFrame implements Runnable {
    public static Window window = null;
    private boolean isRunning;

    public int currentState;
    public Scene currentScene;

    public KL keyListener = new KL();
    public ML mouseListener = new ML();

    Window(int width, int height, String title) {
        setTitle(title);
        setSize(width, height);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setUndecorated(true);
        setVisible(true);
        isRunning = true;
        addKeyListener(keyListener);
        addMouseListener(mouseListener);
        addMouseMotionListener(mouseListener);
        this.changeState(0); //start to the main menu
    }

    public static Window getWindow() {
        if(window == null) {
            window = new Window(Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT, Constants.SCREEN_TITLE);
        }
        return window;
    }

    public void close() {
        isRunning = false;
    }

    public void changeState(int state) {
        this.currentState = state;
        switch (this.currentState) {
            case 0:
                this.currentScene = new MainMenu(keyListener, mouseListener);
                break;
            case 1:
                this.currentScene = new GameScene(keyListener);
                break;
            case 2:
                this.currentScene = new AiFrame(new Board(25,25));
                break;
            default:
                this.currentScene = null;
                System.out.println("Unknown scene");
                break;
        }
    }

    private void update(double deltaTime) {
        Image dbImage = createImage(getWidth(), getHeight());
        Graphics dbGraphics = dbImage.getGraphics();
        this.draw(dbGraphics);
        getGraphics().drawImage(dbImage, 0, 0, this);
        currentScene.update(deltaTime);
    }

    public void draw(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        currentScene.draw(g2d);
    }

    @Override
    public void run() {
        Instant lastFrameTime = Instant.now();
        try {
            while (isRunning) {
                Instant time = Instant.now();
                double deltaTime = Duration.between(lastFrameTime, time).toNanos() * 10E-10;
                lastFrameTime = Instant.now();

                double deltaWanted = 0.0167;
                update(deltaWanted);
                long msToSleep = (long)((deltaWanted - deltaTime) * 1000);
                if (msToSleep > 0) {
                    Thread.sleep(msToSleep);
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        this.dispose();
    }
}
