package game.components;

import ai.Field;

public class Rect {
    public double x;
    public double y;
    public double width;
    public double height;

    public Rect(double x, double y, double width, double height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rect r = (Rect) o;
        return x == r.x &&
                y == r.y;
    }
}
