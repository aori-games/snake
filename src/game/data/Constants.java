package game.data;

public class Constants {
    public static final int SCREEN_WIDTH = 648;
    public static final int SCREEN_HEIGHT = 648;
    public static final String SCREEN_TITLE = "Snake";
    public static final float OG_BETWEEN_TIME = 0.1f;
    public static final float TILE_WIDTH = 24;
}
