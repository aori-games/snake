package game.data;

public enum Direction {
    RIGHT, LEFT, UP, DOWN
}
