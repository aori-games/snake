package game.data;

public enum PoleDirection {
    NORTH, EAST, SOUTH, WEST;
}
