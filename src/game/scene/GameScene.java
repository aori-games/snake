package game.scene;

import game.components.Food;
import game.components.Rect;
import game.components.Snake;
import game.data.Constants;
import game.data.Direction;
import game.listener.KL;


import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.geom.Rectangle2D;

public class GameScene extends Scene {

    private Rect background, foreground;
    public KL keyListener;
    public Snake snake;
    public Food food;

    public GameScene(KL keyListener){
        this.keyListener = keyListener;
        background = new Rect(0,0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
        foreground = new Rect(24,24, Constants.TILE_WIDTH*25, Constants.TILE_WIDTH*25);
        snake = new Snake(10,48,48+24,24,24, foreground);
        food = new Food(foreground,snake,12,12, Color.RED);
        food.spawn();

    }

    @Override
    public void update(double dt) {
        if(keyListener.isKeyPressed(KeyEvent.VK_UP)) {
            snake.changeDirection(Direction.UP);
        } else if(keyListener.isKeyPressed(KeyEvent.VK_DOWN)) {
            snake.changeDirection(Direction.DOWN);
        } else if(keyListener.isKeyPressed(KeyEvent.VK_LEFT)) {
            snake.changeDirection(Direction.LEFT);
        } else if(keyListener.isKeyPressed(KeyEvent.VK_RIGHT)) {
            snake.changeDirection(Direction.RIGHT);
        }

        if(!food.isSpawned) {
            food.spawn();
        }
        food.update(dt);
        snake.update(dt);

    }

    @Override
    public void draw(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(Color.black);
        g2d.fill(new Rectangle2D.Double(background.x, background.y, background.width, background.height));
        g2d.setColor(Color.white);
        g2d.fill(new Rectangle2D.Double(foreground.x, foreground.y, foreground.width, foreground.height));

        snake.draw(g2d);
        food.draw(g2d);
    }
}
