package game.scene;

import game.components.Rect;
import game.components.Text;
import game.data.Constants;
import game.listener.KL;
import game.listener.ML;
import game.Window;


import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

public class MainMenu extends Scene {

    public KL keyListener;
    public BufferedImage title, play, playPressed, exit, exitPressed;
    public Rect playRect, exitRect, titleRect;
    public BufferedImage playCurrentImage, exitCurrentImage;
    private ML mouseListener;
    private Text aiGame;
    public MainMenu(KL keyListener, ML mouseListener) {
        this.keyListener = keyListener;
        this.mouseListener = mouseListener;

        try {
            BufferedImage spriteSheet = ImageIO.read(new File("src/assets/menuSprite.png"));
            title = spriteSheet.getSubimage(0,242,960,240);
            play = spriteSheet.getSubimage(0,121,263,121);
            playPressed = spriteSheet.getSubimage(264,121,263,121);
            exit = spriteSheet.getSubimage(0,0,233,93);
            exitPressed = spriteSheet.getSubimage(264,0,233,93);
            this.aiGame = new Text("Ai game", new Font("Times New Roman", Font.BOLD, 60), Constants.SCREEN_WIDTH / 2.0 - 115, Constants.SCREEN_HEIGHT / 2.0 + 200, Color.WHITE);
        } catch (Exception e) {
            e.printStackTrace();
        }

        playCurrentImage = play;
        exitCurrentImage = exit;

        titleRect = new Rect(240,80, 300,100);
        playRect = new Rect(310,280,150,70);
        exitRect = new Rect(318,355,130,55);
    }

    @Override
    public void update(double dt) {
        if(mouseListener.getX() >= playRect.x && mouseListener.getX() <= playRect.x + playRect.width && mouseListener.getY() >= playRect.y && mouseListener.getY() <= playRect.y + playRect.height) {
            playCurrentImage = playPressed;
            if(mouseListener.isPressed()) {
                Window.getWindow().changeState(1);
            }
        } else {
            playCurrentImage = play;
        }

        if (mouseListener.getX() > aiGame.x && mouseListener.getX() < aiGame.x + aiGame.width &&
                mouseListener.getY() > aiGame.y - aiGame.height / 2 &&
                mouseListener.getY() < aiGame.y + aiGame.height + aiGame.height / 2 && mouseListener.isPressed()) {
            Window.getWindow().changeState(2);
        }

        if(mouseListener.getX() >= exitRect.x && mouseListener.getX() <= exitRect.x + exitRect.width && mouseListener.getY() >= exitRect.y && mouseListener.getY() <= exitRect.y + exitRect.height) {
            exitCurrentImage = exitPressed;
            if(mouseListener.isPressed()) {
                Window.getWindow().close();
            }
        } else {
            exitCurrentImage = exit;
        }
    }

    @Override
    public void draw(Graphics g) {
        Graphics2D graphics2D = (Graphics2D) g;
        g.setColor(new Color(48, 205, 202));
        g.fillRect(0,0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);

        g.drawImage(title,240,80, 300,100,null);
        g.drawImage(playCurrentImage,310,280,150,70,null);
        g.drawImage(exitCurrentImage,318,355,130,55,null);
        this.aiGame.draw(graphics2D);

        if (mouseListener.getX() > aiGame.x && mouseListener.getX() < aiGame.x + aiGame.width &&
                mouseListener.getY() > aiGame.y - aiGame.height / 2 &&
                mouseListener.getY() < aiGame.y + aiGame.height + aiGame.height / 2) {
            aiGame.color = new Color(255, 255, 255,150);
        } else {
            aiGame.color = new Color(255, 255, 255, 255);
        }
    }
}
