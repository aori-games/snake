package game.utils;

public class Arithmetic {
    public static double getLogarithmicScale(int distance) {
        if(distance < 1) {
            return 0;
        }
        return 1 / Math.pow(2, distance);
    }
}
