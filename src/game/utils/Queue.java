package game.utils;

import game.data.Direction;

public class Queue {
    int size;
    int capacity;
    public Direction[] array;

    // Function to create a queue of given capacity
    public Queue(int capacity) {
        this.capacity = capacity;
        this.size = 0;
        this.array = new Direction[this.capacity];
    }

    // Function to check if the queue is empty
    public boolean isEmpty() {
        return array[0] == null;
    }

    // Function to enqueue an item
    public void enqueue(Direction item) {
        if(!isInQueue(item)) {
            this.array[this.size] = item;
            this.size = this.size + 1;
            System.out.println(item + " enqueued to queue");
        }
    }

    // Function to dequeue an item
    public void dequeue() {
        System.out.println(array[0] + " dequeued to queue");
        for (int i = 1; i < this.capacity; i++) {
            this.array[i - 1] = this.array[i];
        }
        size--;
    }

    // Function to get the front item of the queue
    public Direction front() {
        return this.array[0];
    }

    public boolean isInQueue(Direction direction) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == direction) {
                return true;
            }
        }
        return false;
    }
}
    